#!/usr/bin/env python3
# -*-coding:Utf-8 -*

from configuration import *
from log import *

import re
import time

import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')

import geopandas as gpd
import fiona

def color_name(x,end,color):
    if re.search(r'^'+end,x):
        logging.debug(x)
        return color
    else:
        return 'none'

def main():

    places=['France']
    # EPSG projection number
    projection = 2154
    dict={}
    dict_sort={}

    # Read the shp file
    logging.info('Open file')
    try:
        france = gpd.read_file('../data/France/FRA_adm/FRA_adm5.shp')
    except fiona.errors.DriverError as e:
        exit(1)

    # Sort file
    france = france[france['ENGTYPE_5'].isin(['Commune'])]
    # Project to EPSG projection
    france.to_crs(projection, inplace=True)

    # Dict of prefixe and color to plot on map
    dict_sort={
    'cha'   : '#f5925d',
    'ca'    : '#00b159',
    'plou'  : '#b10058',
    'k'     : '#00aedb',
    'w'     : 'red'
    }

    # Create the graph, set to no axis
    fig, ax = plt.subplots(figsize=(options.size,options.size))
    plt.axis('off')
    for key,value in dict_sort.items():

            logging.info('{}-{}'.format(key,value))
            # Color the city polygon with the color defined if it has the prefixe
            ec = [color_name(str(row['NAME_5']).lower(),key,value) for index, row in france.iterrows()]

            logging.info('Plot map')
            france.plot(ax=ax, facecolor=ec,edgecolor='black',linewidth=0.01)

    logging.info('Save fig to file')

    outPath='{}/{}_{}.png'.format(outDir,str(places[0]),str(time.time()))
    fig.savefig(outPath,dpi=options.dpi,papertype=None,facecolor='#eeeeee',
    transparent=True, bbox_inches='tight',metadata=None)
    plt.close(fig=fig)

if __name__ == '__main__':
    try:
        main()
        exit(0)
    except KnownError as error:
        logger.critical(error)
        exit(error.errorLevel)
    except Exception:
        logger.exception("Unknown Error :")
        logger.error(
            "Thank's to report at "
            "https://framagit.org/Syl20/gispy",
        )
        exit(1)
