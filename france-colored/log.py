#!/usr/bin/env python3
# -*-coding:Utf-8 -*

# log.py

import configuration
import logging

class KnownError(Exception):
    def __init__(self, message, errorLevel):
        # Call the base class constructor with the parameters it needs
        super().__init__(message)
        # Now for your custom code...
        self.errorLevel = errorLevel

# Create logger object
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

# Format of the log
formatter=logging.Formatter('%(asctime)s [%(levelname)s] %(message)s.',datefmt='%Y %b %d %H:%M:%S %Z')

# Create file logger from configuration, set level to DEBUG (=all), specify format and add it to the logger object
log_to_file=logging.FileHandler(configuration.logPath)
log_to_file.setLevel(logging.DEBUG)
log_to_file.setFormatter(formatter)
logger.addHandler(log_to_file)

# Create screen logger, choose level, specify format and add it to the logger object
log_to_screen=logging.StreamHandler()
if configuration.options.verbose:
    log_to_screen.setLevel(logging.INFO)
elif configuration.options.debug:
    log_to_screen.setLevel(logging.DEBUG)
else:
    log_to_screen.setLevel(logging.ERROR)
log_to_screen.setFormatter(formatter)
logger.addHandler(log_to_screen)
