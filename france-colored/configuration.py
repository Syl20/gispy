#!/usr/bin/env python3
# -*-coding:Utf-8 -*

# configuration.py

from optparse import OptionParser
import os

logFile='run.log'
logDir='log'
try:
    os.mkdir(logDir)
except FileExistsError:
    pass
logPath='{}/{}'.format(logDir,logFile)

outDir="carte"
try:
    os.mkdir(outDir)
except FileExistsError:
    pass

defaultSize = 15
alpha=1
zorder = 0
linewidth=0.2
ec = 'black'
bc = 'grey'
network_type='none'
infrastructure='way[highway]'

language='FRA'

tag = {
    'park':     ('leisure',['park', 'playground']),
    'river':    ('water',['river']),
    'water':    ('natural',['water']),
    'glacier':  ('natural',['glacier']),
    'forest':   ('landuse',['forest']),
    'wood':     ('natural',['wood']),
    'building': ('building','all'),
    'coastline':('natural',['coastline']),
    'tree':     ('natural',['tree']),
    'riverbank':('waterway',['riverbank']),
    'fast-food':('amenity',['fast_food']),
    'toilet':   ('amenity',['toilets'])
}

color_list=['#ff0000','#00d5db','#09ff00','#2600d0','#f0ff00','#5cdaff']
green = '#307603'
blue = '#3c84ff'
ice = '#7ee9ff'


def options_parser():

    usage = 'usage : %prog [options]'
    description = 'Compress picture and send to remote dav location.'
    example = 'some examples'
    opt = OptionParser(usage=usage,description=description,epilog=example)
    opt.add_option('-p','--prefix',action='store',type=str,
                    help='Prefix for out file naming.')
    opt.add_option('--dpi',action='store',type=int,default=600,
                    help='dpi for out file.')
    opt.add_option('-s','--size',action='store',type=int,default=defaultSize,
                    help='Size in inch. Default is {}.'.format(str(defaultSize)))
    opt.add_option('-v','--verbose',action='store_true',
                    help='Verbose mode.')
    opt.add_option('-d','--debug',action='store_true',default=False,
                    help='Debug mode.')
    opt.add_option('--progress', action='store_true',
                    help='Show progress')
    opt.add_option('--background', action='store',type=str,default='white',
                    help='background color')

    (options,args) = opt.parse_args()
    return (options,args)

(options,args) = options_parser()
